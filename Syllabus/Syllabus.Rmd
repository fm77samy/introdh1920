---
title: "Syllabus"
author: "Dr. Thomas Koentges"
date: "10/13/2019"
always_allow_html: yes
output:
  html_document: default
  pdf_document: default
  md_document:
    variant: markdown_github
knit: (function(inputFile, encoding) {
  rmarkdown::render(inputFile, encoding = encoding,
  output_dir = "output", output_format = "all") })
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(kableExtra)
```

### Contact the Lecturer

Dr. Thomas Köntges  
Akademischer Assistent, University of Leipzig  
Fellow for Historical Language Processing and Data Analysis, CHS, Havard University  

Phone: 0341 - 97 32348  
Email: <thomas.koentges@uni-leipzig.de>  

Twitter: 	@ThomasKoentges  
GitHub: 	<https://github.com/ThomasK81> 
Website: 	<http://www.thomaskoentges.io>  

Lectures in: B.Sc. and M.Sc.  
Can supervise: Bachelor and Master theses  
Office hours: Tue 11:00 - 13:00, Paulinum Raum P-618  

### Course Resources

* GitLab: <https://git.informatik.uni-leipzig.de/koentges/introdh1920>
* GDrive: <https://drive.google.com/drive/folders/15YGTU5ECcNwU8TbnRwWhL_8UojKoRAL8?usp=sharing>
* RStudioCloud: <https://rstudio.cloud>

### Class Venues

* V: Lecture, Felix-Klein-Hörsaal P501 P5.014, Mondays 1.15-2.45pm
* S: Seminar, 
  * Group A, Seminarraum S412, Tuesdays, 9.15–10.45pm 
(currently 30 students / space for 36)
  * Group B, Rechnerlabor P801, Tuesdays, 3.15–4.45pm 	(currently 40 students / space for 40 (it’s gonna be tight!))
  * Group C, Seminarraum S214, Thursdays, 7.15–8.45pm 	(currently 11 students / space for 40)
* P: Praktikum / Project (Vorlesungsfreie Zeit: Group Work & Project Report)

* Block Seminar (4 appointments; choose one):
  * Group I, 28.10. (Monday), 17:15-21:00, Seminarraum S-314
  * Group II, 2.11. (Saturday), 9:15-13:00, Rechnerlabor P502
  * Group III, 4.11. (Monday), 17:15-21:00, Seminarraum S-314
  * Group IV, 9.11. (Saturday), 9:15-13:00, Rechnerlabor P502 P5.02

### Expectations for Class Participation

* Physical presence in the lectures
* Preparation for and active participation in the seminars
* The assignments given in the seminars are due 48h before the next seminar
* Handing-in of the project report by Feb 29, 2020 (Prüfungsleistung)

### Quick Admin Questions

<https://docs.google.com/forms/d/e/1FAIpQLSfYAm6EBtFuwcfJjzymBDKO2VIzEN-XyPXlsBZ34eJVklI4MQ/viewform?usp=sf_link>

### Schedule

```{r, echo=FALSE}
syllabus <- readr::read_csv("Syllabus 19_20,IntroDH LongFormat.csv", col_types = readr::cols())
kable(syllabus, caption = "Schedule for this semester") %>%
  kable_styling("striped", full_width = F) %>%
  pack_rows("October", 1, 10) %>%
  pack_rows("November", 11, 29) %>%
  pack_rows("December", 30, 41)
```

### About this document

This syllabus is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.