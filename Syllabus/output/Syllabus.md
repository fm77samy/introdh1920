### Contact the Lecturer

Dr. Thomas Köntges  
Akademischer Assistent, University of Leipzig  
Fellow for Historical Language Processing and Data Analysis, CHS, Havard
University

Phone: 0341 - 97 32348  
Email:
<a href="mailto:thomas.koentges@uni-leipzig.de" class="email">thomas.koentges@uni-leipzig.de</a>

Twitter: @ThomasKoentges  
GitHub:
<a href="https://github.com/ThomasK81" class="uri">https://github.com/ThomasK81</a>
Website:
<a href="http://www.thomaskoentges.io" class="uri">http://www.thomaskoentges.io</a>

Lectures in: B.Sc. and M.Sc.  
Can supervise: Bachelor and Master theses  
Office hours: Tue 11:00 - 13:00, Paulinum Raum P-618

### Course Resources

-   GitLab:
    <a href="https://git.informatik.uni-leipzig.de/koentges/introdh1920" class="uri">https://git.informatik.uni-leipzig.de/koentges/introdh1920</a>
-   GDrive:
    <a href="https://drive.google.com/drive/folders/15YGTU5ECcNwU8TbnRwWhL_8UojKoRAL8?usp=sharing" class="uri">https://drive.google.com/drive/folders/15YGTU5ECcNwU8TbnRwWhL_8UojKoRAL8?usp=sharing</a>
-   RStudioCloud:
    <a href="https://rstudio.cloud" class="uri">https://rstudio.cloud</a>

### Class Venues

-   V: Lecture, Felix-Klein-Hörsaal P501 P5.014, Mondays 1.15-2.45pm
-   S: Seminar,
    -   Group A, Seminarraum S412, Tuesdays, 9.15–10.45pm (currently 30
        students / space for 36)
    -   Group B, Rechnerlabor P801, Tuesdays, 3.15–4.45pm (currently 40
        students / space for 40 (it’s gonna be tight!))
    -   Group C, Seminarraum S214, Thursdays, 7.15–8.45pm (currently 11
        students / space for 40)
-   P: Praktikum / Project (Vorlesungsfreie Zeit: Group Work & Project
    Report)

-   Block Seminar (4 appointments; choose one):
    -   Group I, 28.10. (Monday), 17:15-21:00, Seminarraum S-314
    -   Group II, 2.11. (Saturday), 9:15-13:00, Rechnerlabor P502
    -   Group III, 4.11. (Monday), 17:15-21:00, Seminarraum S-314
    -   Group IV, 9.11. (Saturday), 9:15-13:00, Rechnerlabor P502 P5.02

### Expectations for Class Participation

-   Physical presence in the lectures
-   Preparation for and active participation in the seminars
-   The assignments given in the seminars are due 48h before the next
    seminar
-   Handing-in of the project report by Feb 29, 2020 (Prüfungsleistung)

### Quick Admin Questions

<a href="https://docs.google.com/forms/d/e/1FAIpQLSfYAm6EBtFuwcfJjzymBDKO2VIzEN-XyPXlsBZ34eJVklI4MQ/viewform?usp=sf_link" class="uri">https://docs.google.com/forms/d/e/1FAIpQLSfYAm6EBtFuwcfJjzymBDKO2VIzEN-XyPXlsBZ34eJVklI4MQ/viewform?usp=sf_link</a>

### Schedule

<table class="table table-striped" style="width: auto !important; margin-left: auto; margin-right: auto;">
<caption>
Schedule for this semester
</caption>
<thead>
<tr>
<th style="text-align:right;">
Week
</th>
<th style="text-align:right;">
Year
</th>
<th style="text-align:right;">
Month
</th>
<th style="text-align:right;">
Day
</th>
<th style="text-align:left;">
Date
</th>
<th style="text-align:right;">
StartTime
</th>
<th style="text-align:right;">
EndTime
</th>
<th style="text-align:left;">
Type
</th>
<th style="text-align:left;">
Place
</th>
<th style="text-align:left;">
Content
</th>
</tr>
</thead>
<tbody>
<tr grouplength="10">
<td colspan="10" style="border-bottom: 1px solid;">
<strong>October</strong>
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
1
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:right;">
14
</td>
<td style="text-align:left;">
14.10.
</td>
<td style="text-align:right;">
1315
</td>
<td style="text-align:right;">
1445
</td>
<td style="text-align:left;">
Lecture
</td>
<td style="text-align:left;">
Felix-Klein-Hörsaal P501 P5.014
</td>
<td style="text-align:left;">
Everything you wanted to know about DH (Manuel says “Hi”?, also
Syllabus)
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
1
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:right;">
15
</td>
<td style="text-align:left;">
15.10.
</td>
<td style="text-align:right;">
915
</td>
<td style="text-align:right;">
1045
</td>
<td style="text-align:left;">
Seminar A
</td>
<td style="text-align:left;">
Seminarraum S412 S 4.205
</td>
<td style="text-align:left;">
Data Formats & Data Cleaning without programming languages
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
1
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:right;">
15
</td>
<td style="text-align:left;">
15.10.
</td>
<td style="text-align:right;">
1515
</td>
<td style="text-align:right;">
1645
</td>
<td style="text-align:left;">
Seminar B
</td>
<td style="text-align:left;">
Rechnerlabor P801 P8.011
</td>
<td style="text-align:left;">
Data Formats & Data Cleaning without programming languages
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
1
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:right;">
15
</td>
<td style="text-align:left;">
15.10.
</td>
<td style="text-align:right;">
1915
</td>
<td style="text-align:right;">
2045
</td>
<td style="text-align:left;">
Seminar C
</td>
<td style="text-align:left;">
Seminarraum S214 S 2.206
</td>
<td style="text-align:left;">
Data Formats & Data Cleaning without programming languages
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
2
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:right;">
21
</td>
<td style="text-align:left;">
21.10.
</td>
<td style="text-align:right;">
1315
</td>
<td style="text-align:right;">
1445
</td>
<td style="text-align:left;">
Lecture
</td>
<td style="text-align:left;">
Felix-Klein-Hörsaal P501 P5.014
</td>
<td style="text-align:left;">
Which language do you speak? Overview of programming languages used in
Digital Humanities
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
2
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:right;">
22
</td>
<td style="text-align:left;">
22.10.
</td>
<td style="text-align:right;">
915
</td>
<td style="text-align:right;">
1045
</td>
<td style="text-align:left;">
Seminar A
</td>
<td style="text-align:left;">
Seminarraum S412 S 4.205
</td>
<td style="text-align:left;">
Using APIs
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
2
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:right;">
22
</td>
<td style="text-align:left;">
22.10.
</td>
<td style="text-align:right;">
1515
</td>
<td style="text-align:right;">
1645
</td>
<td style="text-align:left;">
Seminar B
</td>
<td style="text-align:left;">
Rechnerlabor P801 P8.011
</td>
<td style="text-align:left;">
Using APIs
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
2
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:right;">
22
</td>
<td style="text-align:left;">
22.10.
</td>
<td style="text-align:right;">
1915
</td>
<td style="text-align:right;">
2045
</td>
<td style="text-align:left;">
Seminar C
</td>
<td style="text-align:left;">
Seminarraum S214 S 2.206
</td>
<td style="text-align:left;">
Using APIs
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
3
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:right;">
28
</td>
<td style="text-align:left;">
28.10.
</td>
<td style="text-align:right;">
1315
</td>
<td style="text-align:right;">
1445
</td>
<td style="text-align:left;">
Lecture
</td>
<td style="text-align:left;">
Felix-Klein-Hörsaal P501 P5.014
</td>
<td style="text-align:left;">
Linked Data & Microservices in the Humanities
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
3
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:right;">
28
</td>
<td style="text-align:left;">
28.10.
</td>
<td style="text-align:right;">
1715
</td>
<td style="text-align:right;">
2100
</td>
<td style="text-align:left;">
Block Seminar, Group I
</td>
<td style="text-align:left;">
Seminarraum S-314
</td>
<td style="text-align:left;">
Building Web Applications for DH
</td>
</tr>
<tr grouplength="19">
<td colspan="10" style="border-bottom: 1px solid;">
<strong>November</strong>
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
3
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
2
</td>
<td style="text-align:left;">
2.11.
</td>
<td style="text-align:right;">
915
</td>
<td style="text-align:right;">
1300
</td>
<td style="text-align:left;">
Block Seminar, Group II
</td>
<td style="text-align:left;">
Rechnerlabor P502 P5.02
</td>
<td style="text-align:left;">
Building Web Applications for DH
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
4
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
4
</td>
<td style="text-align:left;">
4.11.
</td>
<td style="text-align:right;">
1315
</td>
<td style="text-align:right;">
1445
</td>
<td style="text-align:left;">
Lecture
</td>
<td style="text-align:left;">
Felix-Klein-Hörsaal P501 P5.014
</td>
<td style="text-align:left;">
Natural Language Processing for Historical Languages (and other
languages too)
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
4
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
4
</td>
<td style="text-align:left;">
4.11.
</td>
<td style="text-align:right;">
1715
</td>
<td style="text-align:right;">
2100
</td>
<td style="text-align:left;">
Block Seminar, Group III
</td>
<td style="text-align:left;">
Seminarraum S-314
</td>
<td style="text-align:left;">
Building Web Applications for DH
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
4
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
5
</td>
<td style="text-align:left;">
5.11.
</td>
<td style="text-align:right;">
915
</td>
<td style="text-align:right;">
1045
</td>
<td style="text-align:left;">
Seminar A
</td>
<td style="text-align:left;">
Seminarraum S412 S 4.205
</td>
<td style="text-align:left;">
Working with High-Dimensional Data
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
4
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
5
</td>
<td style="text-align:left;">
5.11.
</td>
<td style="text-align:right;">
1515
</td>
<td style="text-align:right;">
1645
</td>
<td style="text-align:left;">
Seminar B
</td>
<td style="text-align:left;">
Rechnerlabor P801 P8.011
</td>
<td style="text-align:left;">
Working with High-Dimensional Data
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
4
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
5
</td>
<td style="text-align:left;">
5.11.
</td>
<td style="text-align:right;">
1915
</td>
<td style="text-align:right;">
2045
</td>
<td style="text-align:left;">
Seminar C
</td>
<td style="text-align:left;">
Seminarraum S214 S 2.206
</td>
<td style="text-align:left;">
Working with High-Dimensional Data
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
4
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
9
</td>
<td style="text-align:left;">
9.11.
</td>
<td style="text-align:right;">
915
</td>
<td style="text-align:right;">
1300
</td>
<td style="text-align:left;">
Block Seminar, Group IV
</td>
<td style="text-align:left;">
Rechnerlabor P502 P5.02
</td>
<td style="text-align:left;">
Building Web Applications for DH
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
5
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:left;">
11.11.
</td>
<td style="text-align:right;">
1315
</td>
<td style="text-align:right;">
1445
</td>
<td style="text-align:left;">
Lecture
</td>
<td style="text-align:left;">
Felix-Klein-Hörsaal P501 P5.014
</td>
<td style="text-align:left;">
Topic Modelling & Word Vector Embedding
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
5
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:left;">
12.11.
</td>
<td style="text-align:right;">
915
</td>
<td style="text-align:right;">
1045
</td>
<td style="text-align:left;">
Seminar A
</td>
<td style="text-align:left;">
Seminarraum S412 S 4.205
</td>
<td style="text-align:left;">
Topic Modelling
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
5
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:left;">
12.11.
</td>
<td style="text-align:right;">
1515
</td>
<td style="text-align:right;">
1645
</td>
<td style="text-align:left;">
Seminar B
</td>
<td style="text-align:left;">
Rechnerlabor P801 P8.011
</td>
<td style="text-align:left;">
Topic Modelling
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
5
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:left;">
12.11.
</td>
<td style="text-align:right;">
1915
</td>
<td style="text-align:right;">
2045
</td>
<td style="text-align:left;">
Seminar C
</td>
<td style="text-align:left;">
Seminarraum S214 S 2.206
</td>
<td style="text-align:left;">
Topic Modelling
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
6
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
18
</td>
<td style="text-align:left;">
18.11.
</td>
<td style="text-align:right;">
1315
</td>
<td style="text-align:right;">
1445
</td>
<td style="text-align:left;">
Lecture
</td>
<td style="text-align:left;">
Felix-Klein-Hörsaal P501 P5.014
</td>
<td style="text-align:left;">
Working with Geospatial Data
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
6
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
19
</td>
<td style="text-align:left;">
19.11.
</td>
<td style="text-align:right;">
915
</td>
<td style="text-align:right;">
1045
</td>
<td style="text-align:left;">
Seminar A
</td>
<td style="text-align:left;">
Seminarraum S412 S 4.205
</td>
<td style="text-align:left;">
Working with Geospatial Data
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
6
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
19
</td>
<td style="text-align:left;">
19.11.
</td>
<td style="text-align:right;">
1515
</td>
<td style="text-align:right;">
1645
</td>
<td style="text-align:left;">
Seminar B
</td>
<td style="text-align:left;">
Rechnerlabor P801 P8.011
</td>
<td style="text-align:left;">
Working with Geospatial Data
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
6
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
19
</td>
<td style="text-align:left;">
19.11.
</td>
<td style="text-align:right;">
1915
</td>
<td style="text-align:right;">
2045
</td>
<td style="text-align:left;">
Seminar C
</td>
<td style="text-align:left;">
Seminarraum S214 S 2.206
</td>
<td style="text-align:left;">
Working with Geospatial Data
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
7
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
25
</td>
<td style="text-align:left;">
25.11.
</td>
<td style="text-align:right;">
1315
</td>
<td style="text-align:right;">
1445
</td>
<td style="text-align:left;">
Lecture
</td>
<td style="text-align:left;">
Felix-Klein-Hörsaal P501 P5.014
</td>
<td style="text-align:left;">
Social Network Analysis
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
7
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
26
</td>
<td style="text-align:left;">
26.11.
</td>
<td style="text-align:right;">
915
</td>
<td style="text-align:right;">
1045
</td>
<td style="text-align:left;">
Seminar A
</td>
<td style="text-align:left;">
Seminarraum S412 S 4.205
</td>
<td style="text-align:left;">
Social Network Analysis
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
7
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
26
</td>
<td style="text-align:left;">
26.11.
</td>
<td style="text-align:right;">
1515
</td>
<td style="text-align:right;">
1645
</td>
<td style="text-align:left;">
Seminar B
</td>
<td style="text-align:left;">
Rechnerlabor P801 P8.011
</td>
<td style="text-align:left;">
Social Network Analysis
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
7
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
11
</td>
<td style="text-align:right;">
26
</td>
<td style="text-align:left;">
26.11.
</td>
<td style="text-align:right;">
1915
</td>
<td style="text-align:right;">
2045
</td>
<td style="text-align:left;">
Seminar C
</td>
<td style="text-align:left;">
Seminarraum S214 S 2.206
</td>
<td style="text-align:left;">
Social Network Analysis
</td>
</tr>
<tr grouplength="12">
<td colspan="10" style="border-bottom: 1px solid;">
<strong>December</strong>
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
8
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:right;">
2
</td>
<td style="text-align:left;">
2.12.
</td>
<td style="text-align:right;">
0
</td>
<td style="text-align:right;">
2399
</td>
<td style="text-align:left;">
No Class
</td>
<td style="text-align:left;">
No Class
</td>
<td style="text-align:left;">
Dies Academicus: Which DH Projects interest you
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
8
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:right;">
3
</td>
<td style="text-align:left;">
3.12.
</td>
<td style="text-align:right;">
915
</td>
<td style="text-align:right;">
1045
</td>
<td style="text-align:left;">
Seminar A
</td>
<td style="text-align:left;">
Seminarraum S412 S 4.205
</td>
<td style="text-align:left;">
Student Presentations / Discussion
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
8
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:right;">
3
</td>
<td style="text-align:left;">
3.12.
</td>
<td style="text-align:right;">
1515
</td>
<td style="text-align:right;">
1645
</td>
<td style="text-align:left;">
Seminar B
</td>
<td style="text-align:left;">
Rechnerlabor P801 P8.011
</td>
<td style="text-align:left;">
Student Presentations / Discussion
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
8
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:right;">
3
</td>
<td style="text-align:left;">
3.12.
</td>
<td style="text-align:right;">
1915
</td>
<td style="text-align:right;">
2045
</td>
<td style="text-align:left;">
Seminar C
</td>
<td style="text-align:left;">
Seminarraum S214 S 2.206
</td>
<td style="text-align:left;">
Student Presentations / Discussion
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
9
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:right;">
9
</td>
<td style="text-align:left;">
9.12.
</td>
<td style="text-align:right;">
1315
</td>
<td style="text-align:right;">
1445
</td>
<td style="text-align:left;">
Lecture
</td>
<td style="text-align:left;">
Felix-Klein-Hörsaal P501 P5.014
</td>
<td style="text-align:left;">
Challenges of Data Curation & Corpus Building
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
9
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:left;">
10.12.
</td>
<td style="text-align:right;">
915
</td>
<td style="text-align:right;">
1045
</td>
<td style="text-align:left;">
Seminar A
</td>
<td style="text-align:left;">
Seminarraum S412 S 4.205
</td>
<td style="text-align:left;">
Building a Data Reading Environment
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
9
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:left;">
10.12.
</td>
<td style="text-align:right;">
1515
</td>
<td style="text-align:right;">
1645
</td>
<td style="text-align:left;">
Seminar B
</td>
<td style="text-align:left;">
Rechnerlabor P801 P8.011
</td>
<td style="text-align:left;">
Building a Data Reading Environment
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
9
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:right;">
10
</td>
<td style="text-align:left;">
10.12.
</td>
<td style="text-align:right;">
1915
</td>
<td style="text-align:right;">
2045
</td>
<td style="text-align:left;">
Seminar C
</td>
<td style="text-align:left;">
Seminarraum S214 S 2.206
</td>
<td style="text-align:left;">
Building a Data Reading Environment
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
10
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:right;">
16
</td>
<td style="text-align:left;">
16.12.
</td>
<td style="text-align:right;">
1315
</td>
<td style="text-align:right;">
1445
</td>
<td style="text-align:left;">
Lecture
</td>
<td style="text-align:left;">
Felix-Klein-Hörsaal P501 P5.014
</td>
<td style="text-align:left;">
Citizen Science in DH
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
10
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:right;">
17
</td>
<td style="text-align:left;">
17.12.
</td>
<td style="text-align:right;">
915
</td>
<td style="text-align:right;">
1045
</td>
<td style="text-align:left;">
Seminar A
</td>
<td style="text-align:left;">
Seminarraum S412 S 4.205
</td>
<td style="text-align:left;">
Citizen Science in DH
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
10
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:right;">
17
</td>
<td style="text-align:left;">
17.12.
</td>
<td style="text-align:right;">
1515
</td>
<td style="text-align:right;">
1645
</td>
<td style="text-align:left;">
Seminar B
</td>
<td style="text-align:left;">
Rechnerlabor P801 P8.011
</td>
<td style="text-align:left;">
Citizen Science in DH
</td>
</tr>
<tr>
<td style="text-align:right; padding-left: 2em;" indentlevel="1">
10
</td>
<td style="text-align:right;">
2019
</td>
<td style="text-align:right;">
12
</td>
<td style="text-align:right;">
17
</td>
<td style="text-align:left;">
17.12.
</td>
<td style="text-align:right;">
1915
</td>
<td style="text-align:right;">
2045
</td>
<td style="text-align:left;">
Seminar C
</td>
<td style="text-align:left;">
Seminarraum S214 S 2.206
</td>
<td style="text-align:left;">
Citizen Science in DH
</td>
</tr>
</tbody>
</table>
### About this document

This syllabus is an R Markdown document. Markdown is a simple formatting
syntax for authoring HTML, PDF, and MS Word documents. For more details
on using R Markdown see
<a href="http://rmarkdown.rstudio.com" class="uri">http://rmarkdown.rstudio.com</a>.
